# Docker

使用`docker-compose`建立本地開發環境

## 系統版本

專案:
- nginx:alpine
- php7.3
- mariaDB10.5.15
- redis:latest

本地:
- docker
- git
- php7.3 & composer
- nvm / node 14.15.4 / npm 6.14

## 建置步驟

#### 專案 Git clone and *install

- [admin-backoffice](http://gitlab-sbolive.remotes.local/sbo-live/admin-backoffice)
- [player-h5](http://gitlab-sbolive.remotes.local/sbo-live/player-h5)
- [sbo_live_chatroom](http://gitlab-sbolive.remotes.local/sbo-live/sbo_live_chatroom)
- [sbo_live_php](http://gitlab-sbolive.remotes.local/sbo-live/sbo_live_php)
- [streamer-backoffice](http://gitlab-sbolive.remotes.local/sbo-live/streamer-backoffice)

#### 建立docker環境

1. *install docker
2. 進入本專案主目錄(docker-compose.yml檔所在地)
3. `cp .env.example .env`
4. 修改`.env`檔參數(產品專案所在路徑、DB設定、docker資料&log路徑等)
5. `docker-compose up -d`

#### host 設定

開啟檔案路徑C:\Windows\System32\drivers\etc\hosts，新增以下:  

`127.0.0.1 devadmin.sbolive.com`  
`10.40.3.228 devapi.sbolive.com`  
`127.0.0.1 devstreamer.sbolive.com`  
`127.0.0.1 devgame.sbolive.com`  
`::1  devadmin.sbolive.com`  

> *10.40.3.228 為host.docker.internal IP，為了讓chat呼叫時不至於導到chat的container內部

#### 備註

*install專案:
- sbo_live_php
	- install php7.3 & [composer](https://getcomposer.org/download/)
	- 於專案主目錄下 `cp .env.example .env`
	- 於專案主目錄下 `composer intall`
- admin-backoffice/streamer-backoffice/player-h5
	- install nvm: 下載&安裝[`nvm-setup.exe` ](https://github.com/coreybutler/nvm-windows/releases)
	- 安裝node(用管理員身分執行): `nvm install 14.15.4`
		> 檢查node版本 `node -v`
		> 檢查npm版本 `npm -v`
	- 於各專案主目錄下 `npm install`
	- 於各專案主目錄下 `npm run build:dev`
	> 如使用非cmd(如bash)，遇到CRLF問題，可先改用cmd來build
	> 及時編譯&運行 `npm run start:dev`

*install docker後額外設定Settings:
- General: 取消勾選 **Use the WSL 2 based engine**
- Resources => ADVANCED:
	- Memory: 4GB
	- Disk image size: 100GB
- Resources => FILE SHARING
    - 新增Disk image location
	- 新增專案所在目錄
	- 新增docker data & log 所在目錄(see .env)
